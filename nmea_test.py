# https://docs.python.org/3/library/unittest.html
import unittest
from nmea import nmea_message, util

def coord_from_string(coord:str, hemi: str) -> float:
    """ Format a latitude or longitude correctly

        from my NMEA-Definition:
        $--GGA,hhmmss.ss,llll.lll,A,yyyyy.yyy,B,x,uu,v.v,w.w,M,x.x,M,,zzzz*hh<CR><LF>
        
        llll.lll  | Latitude      | Latitude in ddmm.mmmm format. Leading zeros are inserted.
        A         | N/S Indicator | ‘N’ = North, ‘S’ = South
        yyyyy.yyy | Longitude     | Longitude in dddmm.mmmm format. Leading zeros are inserted.
        B         | E/W Indicator | ‘E’ = East, ‘W’ = West

    --> when copying digits using bytes-range, yields wrong results (because float does not store leading zeros)
    
    :param coord: Latitude or Longitude value as string (including leading 0)
    :param hemi: Hemisphere (W, E, N, S)
    :return: Floating point number positive in northern and eastern hemispheres and negative in southern and western.
    """

    assert isinstance(coord, str)
    assert isinstance(hemi, str)

    if hemi in ['W', 'E']:
        deg_len = 3
    elif hemi in ['N', 'S']:
        deg_len = 2
    else:
        # uh oh
        # python_logger.get_logger().error('Invalid hemisphere specified in coordinate: '+hemi)
        raise ValueError('Invalid hemisphere specified: '+hemi)

    deg = float(str(coord)[0:deg_len])
    min = float(str(coord)[deg_len:])
    angle = deg + min / 60

    if hemi in ['W', 'S']:
        angle = -angle
    return angle

class TestNMEA(unittest.TestCase):
    line = b"$GNGGA,185724.000,3102.2749,N,00941.9214,E,1,04,1.8,275.1,M,50.5,M,,0000*49"
    msg_tokens = []

    def setUp(self) -> None:
        msg = nmea_message.NMEAMessage(self.line)
        self.msg_tokens = nmea_message.NMEAMessage.break_message(msg)
        return super().setUp()

    def test_coord_util(self):
        lat = util.format_coordinate(float(self.msg_tokens[2]), self.msg_tokens[3])
        lon = util.format_coordinate(float(self.msg_tokens[4]), self.msg_tokens[5])
        self.assertEqual(lat, 31.037915)
        self.assertEqual(lon, 9.69869)

    # using string representation of coord
    def test_coord_fron_string(self):
        lat = coord_from_string(self.msg_tokens[2], self.msg_tokens[3])
        lon = coord_from_string(self.msg_tokens[4], self.msg_tokens[5])
        self.assertEqual(lat, 31.037915)
        self.assertEqual(lon, 9.69869)

if __name__ == '__main__':
    unittest.main()